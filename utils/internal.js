const fetchAll = items => items.map(({_doc}) => _doc);
const fetch =  ({_doc}) => _doc;

module.exports = {fetchAll, fetch};
