const moment = require('moment');

const generatorCode = function (label, actionTime, options = {}) {
    const optKey = Object.entries(options).reduce((t, c) =>  t + c.join('='), '');
    const timeKey = moment(actionTime).millisecond(0).format('YYYYMMDDHHmmss');
    return `${timeKey}$${label}$${optKey}`
};

module.exports = {generatorCode};
