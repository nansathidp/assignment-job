const config = require('./config');
const mongoose = require('mongoose');
const moment = require('moment');
const Events = require('events');
const JobEvent = new Events();


config.initMongo();
const init = function () {
    // require('./model/jobs');
    require('./model/register')()
    require('./events/job')(JobEvent);
    require('./events/action')(JobEvent);
    require('./events/update')(JobEvent);
};

const main = function () {
    const fetch = require('./libs/jobs');
    setInterval(function () {
        fetch.getJobRun(moment().milliseconds(0)).then(function (jobs) {
            for (let job of jobs) {
                const {label, data, _id, runAt} = job;
                console.log(label, _id, runAt);
                JobEvent.emit(label, {...data, _id})
            }
        }).catch(console.log)
    }, 1000)
};

const isWaiting = setInterval(function () {
    if (mongoose.connection.readyState === 1) {
        init();
        main();
        clearInterval(isWaiting);
    }
}, 1000);


