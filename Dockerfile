FROM node:12-alpine as base
COPY package.json /src/app/package.json
WORKDIR /src/app/
RUN npm install --production

FROM base
COPY . /src/app/
RUN chmod +x service
