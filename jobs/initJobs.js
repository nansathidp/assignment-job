const moment = require("moment");

const {getEpg} = require('../libs/epgSchedule');
const {getChannelList, getChannelInfo} = require('../libs/channel');
const {generatorCode} = require('../utils/jobs');
const {initJob, getJob, getJobs} = require('../libs/jobs');
const {getEpgList} = require('../libs/record');


async function createJob(ePGs) {
    if (Array.isArray(ePGs) && ePGs.length > 0) {
        for (let epg of ePGs) {
            const {episode_id, start_date, end_date, channel_code} = epg;
            let channel = await getChannelInfo({code: channel_code});
            if (!channel) {
                channel = await getChannelInfo({bms_channel_code: channel_code})
            }
            const {code} = channel;


            const codeStart = generatorCode('start', start_date, {code, action: 'start'});
            const codeStop = generatorCode('stop', end_date, {code, action: 'stop'});
            const codeStatusUp = generatorCode('update', start_date, {code, status: 'record'});
            const codeStatusDown = generatorCode('update', end_date, {code, status: 'recorded'});
            const jobs = await getJobs({code: {$in: [codeStart, codeStop, codeStatusUp, codeStatusDown]}});

            const bodyStart = {
                code: codeStart,
                label: 'action',
                runAt: moment(start_date).milliseconds(0),
                data: {
                    code,
                    action: 'start'
                }
            };
            const bodyStop = {
                code: codeStop,
                label: 'action',
                runAt: moment(end_date).add(-5, 'seconds').milliseconds(0),
                data: {
                    code,
                    action: 'stop'
                }
            };
            const bodyStatusUP = {
                code: codeStatusUp,
                label: 'update',
                runAt: moment(start_date).add(3, 'seconds').milliseconds(0),
                data: {
                    ep_id: episode_id,
                    status: 'record'
                }
            };
            const bodyStatusDown = {
                code: codeStatusDown,
                label: 'update',
                runAt: moment(end_date).milliseconds(0),
                data: {
                    ep_id: episode_id,
                    status: 'recorded'
                }
            };

            try {
                if (Array.isArray(jobs)) {
                    const jobsCode = jobs.map(({code}) => code);
                    if (!jobsCode.includes(codeStart)) {
                        await initJob(bodyStart);
                    }
                   if (!jobsCode.includes(codeStop)) {
                       await initJob(bodyStop);
                   }
                   if (!jobsCode.includes(codeStatusUp)) {
                       await initJob(bodyStatusUP);
                   }
                   if (!jobsCode.includes(codeStatusDown)) {
                       await initJob(bodyStatusDown)
                   }
                }

            } catch (e) {
                console.log(e)
            }
        }
    }

}

async function main(timestamp) {
    try {
        const channels = await getChannelList();
        const ePgs = [];
        for (let channel of channels) {
            const {code, live_to_vod} = channel;
            if (!code) {
                continue
            }
            try {
                let epgSchedulers = await getEpg(code, timestamp);
                if (Array.isArray(epgSchedulers) && epgSchedulers.length > 0) {
                    const episodeIds = epgSchedulers.map(({episode_id}) => episode_id);

                    if (live_to_vod === 'select') {
                        const condition = {ep_id: {$in: episodeIds}, status: 'queue'};
                        let epIds = await getEpgList(condition, {ep_id: 1});
                        epIds = epIds.map(({ep_id}) => ep_id);
                        console.log(epIds)
                        epgSchedulers = epgSchedulers.filter(({episode_id}) => epIds.includes(episode_id))
                    }
                    if (epgSchedulers.length > 0) {
                        epgSchedulers = epgSchedulers.map(item => Object.assign(item, {channel_code: code}));
                        ePgs.push(...epgSchedulers);
                    }
                }

            } catch (e) {
                console.log(e)
            }
        }
        await createJob(ePgs)
    } catch (e) {
        console.log(e);
        process.exit(1)
    }
}

module.exports = {main};
