const {mongoSession} = require('../config');
const model = function (name) {
   return mongoSession.db.collection(name, {raw: true})

}

module.exports = {model};
