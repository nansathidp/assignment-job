const {oracleSession} = require('../config');

const {OBJECT} = require('oracledb');

const responseOptionsWithError = error => error ? console.log(error) : console.log('close connection success');
const doRelease = oracleConnection => oracleConnection ? oracleConnection.release(responseOptionsWithError) : console.log('connect empty.');
const query = async (querySet) => {
    const connection = await oracleSession();
    try {
        const {rows} = await connection.execute(querySet, [], {outFormat: OBJECT});
        doRelease(connection);
        return rows
    } catch (e) {
        doRelease(connection);
        console.log(e);
        throw e
    }
};


module.exports = {query};
