const recordLibs = require('../libs/record');

const update = event => async function ({ep_id, status, _id}) {
    try {
        if (ep_id ===  null){
            console.log(`Record ${ep_id} finished.`);
            event.emit('finish', _id)
        }
        const record = await recordLibs.updateByEpId(ep_id, status);
        await recordLibs.updateHistory(_id);
        console.log(`Record ${record._id} is ${record.status}`);
        if (status === record.status) {
            console.log(`Record ${ep_id} finished.`);
            event.emit('finish', _id)
        } else {
            console.log('Try updating ....');
            event.emit('retry', _id);
        }
    } catch (e) {
        event.emit('retry', _id);
        console.log(e);
        console.log('Try updating ....')
    }
};

module.exports = function (event) {
    event.on('update', update(event))
};
