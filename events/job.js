const jobs = require('../libs/jobs');
const finish = async function (_id) {
    try {
        console.log('event job finish ---> ', _id)
        const result = await jobs.jobFinish({_id})
        console.log(result)
    } catch (e) {
        throw e
    }
};

const retry = async function (_id) {
    try {
        const job = await jobs.getJob({_id});
        if (job.countRun) {
            await jobs.jobRetry({_id}, job.countRun + 1)
        } else {
            await jobs.jobRetry({_id}, 1)
        }
        
    } catch (e) {
        throw e
    }
};

module.exports = function (event) {
    event.on('finish', finish);
    event.on('retry', retry);
};
