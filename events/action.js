const {rabbitSession, queueName} = require('../config');

const action = event => async function ({action, code, _id}) {
    try {
        const session = await rabbitSession();
        const channel = await session.createChannel();
        console.warn(await channel.assertQueue(queueName));
        await channel.sendToQueue(queueName, Buffer.from(JSON.stringify({action, code})));
        setTimeout(function () {
            event.emit('finish', _id);
            console.log(`Send ${code} (${action}) record  finish.`);
            session.close()
        }, 500)
    } catch (e) {
        console.log(e);
        event.emit('retry', _id);
    }
};

module.exports = function (event) {
    event.on('action', action(event));
};
