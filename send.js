const config = require('./config');
const mongoose = require('mongoose')

function init() {
    config.initMongo();
    const isWaiting = setInterval(function () {
        if (mongoose.connection.readyState === 1) {
            create();
            start();
            clearInterval(isWaiting);

        }
    }, 1000);

}

function create() {
    require('./model/register')()
}

function start() {
    const mongoose = require('mongoose');
    const jobModel = mongoose.model('job');
    const moment = require('moment');
    jobModel.deleteMany({"label": 'action', 'data.action': 'start'}).then(function () {
        jobModel.updateMany({
            "label": 'action',
            'data.action': 'stop'
        }, {runAt: moment().add(3, 'seconds')}).then(console.log())
    }).catch(console.log)

}

init();
