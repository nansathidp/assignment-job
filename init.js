const config = require('./config');
const mongoose = require('mongoose');
const moment = require('moment');
config.initMongo();

function init() {
    require('./model/register')()
}

function main() {
    const jobs = require('./jobs/initJobs');

    jobs.main(moment()).catch(console.log);
    setInterval(function () {
        jobs.main(moment().add(15, 'minutes')).catch(console.log)
    }, 1000)
}

const isWaiting = setInterval(function () {
    if (mongoose.connection.readyState === 1) {
        init();
        main();
        clearInterval(isWaiting);

    }
}, 1000);
