const moment = require('moment');
const {query} = require('../core/oracle');
const {timeFormatOracle, timeStrOracle, timeQuery} = require('../config');
const channel = require('./channel');


const getEpg = async function (code, startAt) {
    try {
        const chanelInfo = await channel.getChannelInfo({code});
        if (chanelInfo) {
            let {bms_channel_code} = chanelInfo;
            if (bms_channel_code) {
                bms_channel_code = bms_channel_code.toLocaleUpperCase();
                let endAt = moment(startAt).add(15, 'minutes').format(timeStrOracle);
                startAt = moment(startAt).format(timeStrOracle);
                const sql = `select episode_id "episode_id", start_date "start_date", end_date "end_date", channel_code "channel_code" from sch_bms
                where channel_code='${bms_channel_code}' 
                and start_date 
                BETWEEN TO_DATE('${startAt}', '${timeFormatOracle}') and TO_DATE('${endAt}', '${timeFormatOracle}')`
                return await query(sql)
            }
        }
        return null

    } catch (error) {
        throw error
    }
};

module.exports = {getEpg}
