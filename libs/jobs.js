const moment = require('moment');
const mongoose = require('mongoose');
const jobModel = mongoose.model('job');


const getJobRun = async function (runAt) {
    try {
        return await jobModel.find({runAt: {$lte: runAt},isAssigning: {$exists: false}})
    } catch (error) {
        throw error
    }
};

const getJob = async function (condition) {
    try {
        return await jobModel.findOne(condition);
    } catch (e) {
        throw e
    }
};

const getJobs = async function (condition, fields) {
    try {
        return await jobModel.find(condition);
    } catch (e) {
        throw e
    }
};

const jobFinish = async function (condition) {
    try {
        return await jobModel.deleteOne(condition)
    } catch (error) {
        throw error
    }
};

const jobRetry = async function (condition, countRun) {
    try {
        const body = {runAt: moment().add(1, 'seconds'),countRun, isRetry: true};

        return await jobModel.updateOne(condition, body);
    } catch (e) {
        throw e
    }
};

const jobAssigning = async function (condition) {
    try {
        const body = {isAssigning: true};

        return await jobModel.updateOne(condition, body);
    } catch (e) {
        throw e
    }
};

const initJob = async function (body) {
    try {
        const job = new jobModel(body);
        return await job.save();
    } catch (error) {
        throw error
    }
};

module.exports = {initJob, jobFinish, jobRetry, getJobRun, getJob, getJobs,jobAssigning};
