const mongoose = require('mongoose');
const {fetchAll} = require('../utils/internal');
const recordModel = mongoose.model('live_record');
const historyModel = mongoose.model('live2File_history');
const jobsModel = mongoose.model('job');


const getEpgList = async function (condition, selects = {}) {
    try {
        return await recordModel.find(condition, selects);
        // const epgList = await recordModel.find(condition, selects);
        // return fetchAll(epgList)
    } catch (e) {
        throw e
    }
};

const updateHistory = async function (jobId) {
    try {
        const job = await jobsModel.findOne({_id: jobId});
        if (job) {
            console.log('-------- History --------------------\n')
            console.log(' Job:', job, '\n');
            const {data} = job;
            console.log(' Data:', job, '\n');
            console.log('--------- End History ---------------- \n');
            await historyModel.create(data)
        } else {
            console.log('job is ', job)
        }

    } catch (e) {
        throw e
    }
};

const updateByEpId = async function (ep_id, status = 'record') {
    try {
        let recordEpg = await recordModel.findOne({ep_id});
        if (recordEpg) {
            await recordModel.updateOne({ep_id}, {status});
            recordEpg = await recordModel.findOne({ep_id});
        } else {
            recordEpg = await recordModel.create({ep_id, status});
        }

        await historyModel.findOne({ep_id},).sort({record_date: 1});
        return recordEpg
    } catch (e) {
        throw e
    }
};

module.exports = {updateByEpId, getEpgList, updateHistory};
