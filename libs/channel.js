const mongoose = require('mongoose');
const channelModel = mongoose.model('content_channel', {}, 'content_channel', true);
const {fetchAll,fetch} = require('../utils/internal');

const getChannelList = async function () {
    try {
        const channels = await channelModel.find({live_to_vod: {$in: ['all', 'select']}})
        return fetchAll(channels)
    } catch (e) {
        throw e
    }
};

const getChannelInfo = async function (condition) {
    try {
        const chInfo = await channelModel.findOne(condition);
        return fetch(chInfo)
    } catch (error) {
        throw error
    }
};

module.exports = {getChannelList, getChannelInfo};
