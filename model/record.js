const mongoose = require('mongoose');
const liveRecord = {
    title: {
        type: String,
    },
    title_id: {
        type: String,
    },
    ep_id: {
        type: String,
        required: [true, 'Input ep_id'],
    },
    status: {
        type: String,
        required: [true, 'Input status'],
        // default: 'queue'
    },
};
const history = {
    ...liveRecord,
    stream_code: {
        type: String,
    },
    record_date: {
        type: Date,
    }
};

const liveRecordSchema = new mongoose.Schema(liveRecord);
const historySchema = new mongoose.Schema(history);

module.exports = function () {
    mongoose.model('live_record', liveRecordSchema, 'live_record');
    mongoose.model('live2File_history', historySchema, 'live2File_history');
};
