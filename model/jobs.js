const mongoose = require('mongoose');
let timestamps = require('mongoose-timestamp');
let uniqueValidator = require('mongoose-unique-validator');

let moment = require('moment');

const schema = {
    label: {
        index: true,
        type: String,
        required: [true, 'Validate label is events action ']
    },
    code: {
        type: String,
        unique: true,
        required: [true, 'Validate label is events action ']
    },
    runAt: {
        index: true,
        type: Date,
        required: [true, "Validate runAt is action time"],
        default: moment().milliseconds(0).add(2, 'seconds'),
    },
    data: {
        type: Object,
        default: {}
    },
    isRetry: {
        type: Boolean,
        default: false,
    },
    isAssigning: {
        type: Boolean,
        default: false,
    },
    countRun: {
        type: Number,
        default: 0
    },
};

const JobsSchema = new mongoose.Schema(schema);
JobsSchema.index({'**': 'text'});
JobsSchema.plugin(timestamps);
JobsSchema.plugin(uniqueValidator, {message: 'is already'});

module.exports = mongoose.model('job', JobsSchema);
