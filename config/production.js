const oracle = {
    user: process.env.ORA_USER,
    password: process.env.ORA_PASSWORD,
    connectString: `(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = ${process.env.ORA_HOST})(PORT = ${process.env.ORA_PORT})) (CONNECT_DATA = (SID = ${process.env.ORA_SID}) ) )`,
};

const mongoUri = process.env.MONGO_URI;
const timeQuery = Number.isNaN(Number(process.env.TIME_QR)) ? 5 : Number(process.env.TIME_QR);

const rabbit = {
    host: process.env.MQ_HOST,
    port: process.env.MQ_PORT,
    username: process.env.MQ_USER,
    password: process.env.MQ_PASSWORD,
};
const queueName = process.env.QUEUE_NAME;

module.exports = {oracle, mongoUri, timeQuery, rabbit, queueName};
