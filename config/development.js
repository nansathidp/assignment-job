const oracle = {
    user: 'obmsapp',
    password: 'obms$user_01',
    connectString: `(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.18.14.234)(PORT = 1522)) (CONNECT_DATA = (SID = obmsdb) ) )`,
};
const timeQuery = 5; // minute
const mongoUri = 'mongodb://10.18.14.242:27017/stm';

const rabbit = {
    host: '127.0.0.1',
    port: 5672,
    username: 'guest',
    password: 'guest',
};
const queueName = 'action';
module.exports = {mongoUri, oracle, timeQuery, rabbit, queueName};
