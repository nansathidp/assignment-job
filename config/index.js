const configEnv = require(`./${process.env.NODE_ENV}`);

const timeFormatOracle = 'YYYYMMDD HH24MISS';
const timeStrOracle = `YYYYMMDD HHmmss`;

var mongoSession = {};

const oraclePool = {
    poolMax: 100000 * 2,
    poolMin: 5,
    poolIncrement: 5,
    poolTimeout: 45,
};

function initMongo() {
    const mongoose = require('mongoose');
    console.log('Mongo Uri', configEnv.mongoUri);
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
        poolSize: 10, // Maintain up to 10 socket connections
        socketTimeoutMS: 60000, // Close sockets after 45 seconds of inactivity
        family: 4 // Use IPv4, skip trying IPv6
    };
    mongoose.connect(configEnv.mongoUri, options).catch(function (error) {
        console.log(error)
        process.exit(1)
    });
    mongoose.set('debug', process.env.DEBUG);
    //mongoose.set('useCreateIndex', true);
    //mongoose.set('useFindAndModify', false);
    //mongoose.set('useNewUrlParser', true)

}

const oracleSession = async function () {
    const oracleDb = require('oracledb');
    const {oracle} = configEnv;
    return oracleDb.getConnection({
        ...oracle,
        ...oraclePool,
    })
};
const rabbitUri = function () {
    const {rabbit} = configEnv;
    const {username, password, host, port} = rabbit;
    return `amqp://${username}:${password}@${host}:${port}`;
};

const rabbitSession = function () {
    const uri = rabbitUri();
    const amqp = require('amqplib');
    return amqp.connect(uri)
};

module.exports = {
    initMongo,
    mongoSession,
    oracleSession,
    rabbitUri,
    rabbitSession,
    timeStrOracle,
    timeFormatOracle, ...configEnv
};
